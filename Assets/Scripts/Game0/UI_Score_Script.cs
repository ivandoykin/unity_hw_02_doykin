﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Score_Script : MonoBehaviour
{
    private Text score;
    private int score_int;
    void Start()
    {
        score = GetComponent<Text>();
        score_int = 0;
    }
    public void IncrementScore()
    {
        score_int++;
        score.text = score_int.ToString();
    }
    public void ResetScore()
    {
        score.text = 0.ToString();
    }
}
