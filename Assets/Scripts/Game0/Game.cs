﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game0
{

public class Game : MonoBehaviour
{
  public GameObject target_spawn_placeholder;
  public Hero hero;
  public Target origin;
  public Target true_origin;

  private UI_Time_Script uscript;
  private UI_Score_Script scscript;

  public float start_hero_speed = 0.2f;
  public float step_hero_speed = 0.01f;
  public float max_hero_speed = 1.0f;
  public float distance_between_targets = 1.2f;
  private float idle_time = 3f;
  private float idle_max;
  

  List<Target> targets = new List<Target>();

  int target_count = 1;

  public float laser_time = 1.0f;
  float current_laser_time = 0.0f;

  enum GameState
  {
    GAME,
    FIRE,
    NEXT_LEVEL,
    RESET
  }

  GameState state = GameState.GAME;
  GameState pending_state;

  void Start()
  {
    scscript = GetComponentInChildren<UI_Score_Script>();
    uscript = GetComponentInChildren<UI_Time_Script>();
    SetupGame();
  }

  void SetupGame(bool win = true)
  {
    if(!win)
      target_count = 1; //Reset game
    idle_max += 2f;
    idle_time = idle_max;
    SpawnTargets();
    SetupHeroMovement();
    RandomizeTrueTargetPosition();
    state = GameState.GAME;
  }

  void SetupHeroMovement()
  {
    hero.x_a = targets[0].transform.position.x;
    hero.x_b = targets[targets.Count - 1].transform.position.x;
    hero.speed = start_hero_speed + step_hero_speed * (targets.Count - 1);
    hero.speed = Mathf.Min(hero.speed, max_hero_speed);
  }

  void SpawnTargets()
  {
    foreach(var target in targets)
      GameObject.Destroy(target.gameObject);

    targets.Clear();

    SpawnTarget(true_origin);

    for(int i = 1; i < target_count; ++i)
      SpawnTarget(origin);

    var start_position_x = target_spawn_placeholder.transform.position.x -
      (distance_between_targets * target_count) / 2.0f;

    for(int i = 0; i < targets.Count && target_count > 1; ++i)
    {
      targets[i].transform.position = new Vector3(
        start_position_x + distance_between_targets * (float)i,
        target_spawn_placeholder.transform.position.y,
        target_spawn_placeholder.transform.position.z);
    }

    target_count++;
  }

  void RandomizeTrueTargetPosition()
  {
    int index = Random.Range(0, targets.Count);
    var true_target_position = targets[0].transform.position;
    targets[0].transform.position = targets[index].transform.position;
    targets[index].transform.position = true_target_position;
  }

  void SpawnTarget(Target origin)
  {
    var new_target = Target.Instantiate(origin);
    new_target.transform.position = target_spawn_placeholder.transform.position;
    targets.Add(new_target);
  }

  void ProcessInput()
  {
            if (Input.GetMouseButtonDown(0))
            {
                state = GameState.FIRE;
                foreach (var target in targets)
                {
                    if (!target.is_true_target)
                        continue;

                    if (Mathf.Abs(hero.transform.position.x - target.transform.position.x) < 1.0f)
                    {
                        scscript.IncrementScore();
                        pending_state = GameState.NEXT_LEVEL;
                        return;
                    }
                }

                pending_state = GameState.RESET;
            }
            else
            {
                idle_time -= Time.deltaTime;
            }
  }

  void CheckIdleTime()
        {
            if (idle_time <= 0)
            {
                Debug.Log(idle_time);
                SetupGame(win: false);
            }
        }

  void Update()
  { 
    switch(state)
    {
      case GameState.GAME:
        ProcessInput();
        CheckIdleTime();
        uscript.UI_Update(idle_max, idle_time);
        hero.Tick(Time.time);
        break;
      case GameState.FIRE:
        if(current_laser_time == 0.0f)
          hero.SetLaserState(true);
        current_laser_time += Time.deltaTime;
        if(current_laser_time >= laser_time)
        {
          state = pending_state;
          current_laser_time = 0.0f;
          hero.SetLaserState(false);
        }
        break;
      case GameState.NEXT_LEVEL:
         idle_max += 2f;
         SetupGame();
        break;
      case GameState.RESET:
        scscript.ResetScore();
        idle_max = 3f;
        SetupGame(win: false);
        break;
    }
  }
}

}
