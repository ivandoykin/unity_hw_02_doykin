﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Time_Script : MonoBehaviour
{
    private Image img;
    void Start()
    {
        img = GetComponent<Image>();
    }
    public void UI_Update (float max, float cur)
    {
        float percent = cur / max;
        img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 300f*percent);
    }
}
